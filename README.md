# Spring Boot with Kafka Consumer Example

This Project covers how to use Spring Boot with Spring Kafka to Consume JSON/String message from Kafka topics
## Start Zookeeper
- `bin/zookeeper-server-start.sh config/zookeeper.properties`

## Start Kafka Server
- `bin/kafka-server-start.sh config/server.properties`

## Create Kafka Topic
- `bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic Kafka_Example`
- `bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic Kafka_Example_json`

## Publish to the Kafka Topic via Console
- `bin/kafka-console-producer.sh --broker-list localhost:9092 --topic Kafka_Example`
- `bin/kafka-console-producer.sh --broker-list localhost:9092 --topic Kafka_Example_json`


#mas
https://enmilocalfunciona.io/aprendiendo-apache-kafka-parte-1/
https://enmilocalfunciona.io/aprendiendo-apache-kafka-parte-2/
https://enmilocalfunciona.io/aprendiendo-apache-kafka-parte-3/
https://enmilocalfunciona.io/aprendiendo-apache-kafka-parte-4/

https://www.sohamkamani.com/install-and-run-kafka-locally/


#QUE ES KAFKA
https://www.ibm.com/docs/es/oala/1.3.5?topic=SSPFMY_1.3.5/com.ibm.scala.doc/config/iwa_cnf_scldc_apche_con_c.html
Apache Kafka es un sistema de mensajería y una plataforma completa de streaming y de procesamiento de datos en tiempo real. Nos proporciona la capacidad de publicar y procesar flujos de eventos de forma escalable y tolerante a fallos. Kafka se distribuye bajo la licencia Open Source de la Apache Software Foundation.

#ZOOKEPER
 Es imprescindible configurar ZooKeeper para Kafka. ZooKeeper realiza muchas tareas para Kafka pero, en resumen, podemos decir que ZooKeeper gestiona el estado del clúster de Kafka
 