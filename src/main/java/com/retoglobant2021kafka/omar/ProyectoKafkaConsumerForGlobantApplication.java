package com.retoglobant2021kafka.omar;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProyectoKafkaConsumerForGlobantApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProyectoKafkaConsumerForGlobantApplication.class, args);
	}

}
