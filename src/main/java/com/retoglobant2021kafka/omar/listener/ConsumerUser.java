package com.retoglobant2021kafka.omar.listener;

import com.retoglobant2021kafka.omar.model.User;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

/**
 * Created by Omar Calderon Evangelista
 * User: Omar.Calderon
 * Date: 30/09/2021
 * Time: 12:40 p. m.
 * Change Text
 */
@Service
public class ConsumerUser {

  /**
   * Listener escuchando mensajes de un producer.
   * Recibeun String de un productor
   * @param message
   */
  @KafkaListener(topics = "kafka_globant", groupId = "group_globant_peru")
  public void consume(String message) {
    System.out.println("Consumed message: " + message);
  }

  /**
   * Listener escuchando mensajes de un producer.
   * Recibe un objeto User de un productor
   * @param user
   */
  @KafkaListener(topics = "kafka_globant_json", groupId = "group_globant_peru_json",
          containerFactory = "userKafkaListenerFactory")
  public void consumeJson(User user) {
    System.out.println("Consumed JSON Message: " + user);
  }
}
