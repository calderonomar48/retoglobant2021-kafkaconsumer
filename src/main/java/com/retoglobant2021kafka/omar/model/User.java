package com.retoglobant2021kafka.omar.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by Omar Calderon Evangelista
 * User: Omar.Calderon
 * Date: 30/09/2021
 * Time: 12:12 p. m.
 * Change Text
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class User {

  private String name;

  private String dept;

}
